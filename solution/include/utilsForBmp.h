//
// Created by Zlat on 10/18/2022.
//

#ifndef UNTITLED_UTILSFORBMP_H
#define UNTITLED_UTILSFORBMP_H

#include "../inputOutput/fromToBmp.h"
struct __attribute__((packed)) bmpHeader;
size_t sizeOfPadding(const size_t width);
bool headRead(FILE* file, struct bmpHeader* header);
size_t sizeOfImage(const struct image* image);
size_t sizeOfFile(const struct image* image);
struct bmpHeader createHeader(const struct image* image);

#endif //UNTITLED_UTILSFORBMP_H
