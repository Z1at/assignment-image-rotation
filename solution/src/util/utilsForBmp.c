#include "../inputOutput/fromToBmp.h"


struct __attribute__((packed)) bmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

size_t sizeOfPadding(const size_t width){
    return width % 4;
}

bool headRead(FILE* file, struct bmpHeader* header){
    return fread(header, sizeof(struct bmpHeader), 1, file);
}

size_t sizeOfImage(const struct image* image){
    return (image->width * sizeof(struct pixel) + sizeOfPadding(image->width)) * image->height;
}

size_t sizeOfFile(const struct image* image){
    return sizeOfImage(image) + sizeof(struct bmpHeader);
}

struct bmpHeader createHeader(const struct image* image){
    return (struct bmpHeader){
            .bfType = 19778,
            .bfileSize = sizeOfFile(image),
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeOfImage(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrImportant = 0,
            .biClrUsed = 0
    };
}
