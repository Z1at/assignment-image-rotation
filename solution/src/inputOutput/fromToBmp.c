#include "fromToBmp.h"
#include "../util/utilsForBmp.h"

uint16_t TypeOfBMP = 19778;
struct __attribute__((packed)) bmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

enum readStatus fromBmp(FILE* in, struct image* image){
    struct bmpHeader header = {0};

    if(!headRead(in, &header)){
        return READ_INVALID_HEADER;
    }
    if(header.bfType != TypeOfBMP){
        return READ_INVALID_SIGNATURE;
    }

    *image = createImage(header.biWidth, header.biHeight);
    const size_t padding = sizeOfPadding(image->width);

    for(size_t i = 0; i < image->height; i++){
        for(size_t j = 0; j < image->width; j++){
            if(!fread(&(image->data[findIndex(j, i, image->width)]), sizeof(struct pixel), 1, in)){
                return READ_INVALID_BITS;
            }
        }
        if(padding != 0) {
            if (fseek(in, (long)padding, SEEK_CUR)) {
                return READ_INVALID_PADDING;
            }
        }
    }

    return READ_OK;
}

enum writeStatus toBmp(FILE* out, struct image* image){
    struct bmpHeader header = createHeader(image);

    if(!fwrite(&header, sizeof(struct bmpHeader), 1, out)){
        return WRITE_INVALID_HEADER;
    }

    if(fseek(out, header.bOffBits, SEEK_SET)){
        return WRITE_INVALID_SIGNATURE;
    }

    const uint8_t paddings[3] = {0};
    const size_t padding = sizeOfPadding(image->width);

    if(image->data == NULL){
        return WRITE_INVALID_DATA;
    }

    for(size_t i = 0; i < image->height; i++){
        for(size_t j = 0; j < image->width; j++) {
            if (!fwrite(&image->data[findIndex(j, i, image->width)], sizeof(struct pixel), 1, out)) {
                return WRITE_INVALID_BITS;
            }
        }
        if(padding != 0){
            if(!fwrite(paddings, padding, 1, out)){
                return WRITE_INVALID_PADDING;
            }
        }
    }
    return WRITE_OK;
}
