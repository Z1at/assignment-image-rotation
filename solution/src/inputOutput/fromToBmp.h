#ifndef UNTITLED_FROMTOBMP_H
#define UNTITLED_FROMTOBMP_H

#include "../image/image.h"
#include <stdbool.h>
#include <stdio.h>


enum readStatus  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PADDING
};

enum writeStatus  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_BITS,
    WRITE_INVALID_SIGNATURE,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_DATA,
    WRITE_INVALID_PADDING
};


enum readStatus fromBmp(FILE* in, struct image* image);
enum writeStatus toBmp(FILE* out, struct image* image);
#endif //UNTITLED_FROMTOBMP_H
